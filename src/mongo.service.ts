import { Injectable } from '@nestjs/common';
import { MongoClient, Db } from 'mongodb';

//MongoService 是一個 Injectable 的類別，它使用了 @nestjs/common 中的 Injectable 裝飾器，使其能夠在 NestJS 應用程式中作為依賴注入使用。
@Injectable()
export class MongoService {
  //private db: Db; 是一個私有成員變數，用於存儲 MongoDB 的 Db 物件，它表示連接到的資料庫。
  private db: Db;

  //connect() 方法用於連接到 MongoDB 資料庫。它使用 MongoClient 建立一個連接，並指定 MongoDB 的連接字串 'mongodb://localhost:27017'，
  //以及使用 useUnifiedTopology: true 參數來啟用新的連線引擎。然後，它使用client.connect() 非同步地連接到資料庫，並將連接後的 db 賦值給 this.db。
  async connect(): Promise<void> {
    const client = new MongoClient('mongodb://localhost:27017');

    await client.connect();
    this.db = client.db('mydatabase');
  }

  //getCollection(collectionName: string) 方法用於取得指定名稱的集合（collection）。它使用 this.db.collection(collectionName)
  //從資料庫中獲取指定名稱的集合，並將其返回。
  getCollection(collectionName: string) {
    return this.db.collection(collectionName);
  }
}

//export default DatabaseService; 用於將 DatabaseService 類別作為默認導出，以便在其他檔案中引入和使用該服務。
export default MongoService;
