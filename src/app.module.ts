import { MongoService } from './mongo.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [MongoService],
  exports: [MongoService], // 將 MongoService 導出供外部專案使用
})
export class AppModule {
  constructor(private readonly mongoService: MongoService) {}

  async onModuleInit() {
    await this.mongoService.connect();
  }
}
